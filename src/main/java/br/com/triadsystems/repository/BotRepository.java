package br.com.triadsystems.repository;

import br.com.triadsystems.model.Bot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BotRepository extends JpaRepository<Bot, UUID> {

}
