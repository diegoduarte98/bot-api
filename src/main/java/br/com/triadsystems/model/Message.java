package br.com.triadsystems.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(columnDefinition = "BINARY(16)")
    private UUID conversationId;

    private LocalDateTime timestamp;

    @Column(name = "valueFrom", columnDefinition = "BINARY(16)")
    private UUID from;

    @Column(name = "valueTo", columnDefinition = "BINARY(16)")
    private UUID to;

    private String text;
}
