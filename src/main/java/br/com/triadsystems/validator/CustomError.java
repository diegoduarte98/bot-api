package br.com.triadsystems.validator;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class CustomError {

    @Getter
    private List<CustomErrorDetails> errors = new ArrayList<>();

    public void addFieldError(String field, String message) {
        CustomErrorDetails fieldError = new CustomErrorDetails(field, message);
        errors.add(fieldError);
    }
}
