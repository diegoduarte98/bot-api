package br.com.triadsystems.controller.form;

import br.com.triadsystems.model.Bot;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BotForm {

    @NotNull
    private String name;

    public Bot converter() {
        return Bot.builder()
                .name(name)
                .build();
    }
}
