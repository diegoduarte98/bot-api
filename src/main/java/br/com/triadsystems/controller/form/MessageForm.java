package br.com.triadsystems.controller.form;

import br.com.triadsystems.model.Message;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageForm {

    private UUID conversationId;

    private LocalDateTime timestamp;

    private UUID from;

    private UUID to;

    private String text;

    public Message converter() {
        return Message.builder()
                .conversationId(this.conversationId)
                .timestamp(this.timestamp)
                .from(this.from)
                .to(this.to)
                .text(this.text)
                .build();
    }
}
