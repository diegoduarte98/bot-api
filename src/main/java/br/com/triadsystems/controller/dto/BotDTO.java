package br.com.triadsystems.controller.dto;

import br.com.triadsystems.model.Bot;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BotDTO {

    private UUID id;

    @NotNull
    private String name;

    public BotDTO(Bot bot) {
        this.id = bot.getId();
        this.name = bot.getName();
    }
}
