package br.com.triadsystems.controller.dto;

import br.com.triadsystems.model.Message;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageDTO {

    private UUID id;

    private UUID conversationId;

    private LocalDateTime timestamp;

    private UUID from;

    private UUID to;

    private String text;

    public MessageDTO(Message message) {
        this.id = message.getId();
        this.conversationId = message.getConversationId();
        this.timestamp = message.getTimestamp();
        this.from = message.getFrom();
        this.to = message.getTo();
        this.text = message.getText();
    }

    public static List<MessageDTO> converter(List<Message> messages) {
        return messages.stream().map(MessageDTO::new).collect(Collectors.toList());
    }
}
