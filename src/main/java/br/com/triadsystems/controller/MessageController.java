package br.com.triadsystems.controller;

import br.com.triadsystems.controller.dto.MessageDTO;
import br.com.triadsystems.controller.form.MessageForm;
import br.com.triadsystems.model.Message;
import br.com.triadsystems.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@AllArgsConstructor
@RequestMapping("/messages")
public class MessageController {

	@Autowired
	private MessageService messageService;
	
	@PostMapping
	public ResponseEntity<MessageDTO> save(@RequestBody MessageForm form, UriComponentsBuilder builder) {
		Message message = messageService.save(form.converter());

		URI uri = builder.path("/messages/{id}").buildAndExpand(message.getId()).toUri();
		return ResponseEntity.created(uri).body(new MessageDTO(message));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<MessageDTO> findById(@PathVariable UUID id) {
		Optional<Message> optionalMessage = messageService.findById(id);

		if(optionalMessage.isPresent())
			return ResponseEntity.ok(new MessageDTO(optionalMessage.get()));

		return ResponseEntity.notFound().build();
	}
	
	@GetMapping
	public ResponseEntity<List<MessageDTO>> findByConversationId(@RequestParam UUID conversationId) {
		List<Message> messages = messageService.findByConversationId(conversationId);

		if(messages.isEmpty())
			return ResponseEntity.notFound().build();

		return ResponseEntity.ok(MessageDTO.converter(messages));
	}
}
