package br.com.triadsystems.controller;

import br.com.triadsystems.controller.dto.BotDTO;
import br.com.triadsystems.controller.form.BotForm;
import br.com.triadsystems.model.Bot;
import br.com.triadsystems.service.BotService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;
import java.util.UUID;

@RestController
@AllArgsConstructor
@RequestMapping("/bots")
public class BotController {

	@Autowired
	private BotService botService;

	@PostMapping
	public ResponseEntity<BotDTO> save(@Valid @RequestBody BotForm form, UriComponentsBuilder builder) {
		Bot newBot = botService.save(form.converter());

		URI uri = builder.path("/bots/{id}").buildAndExpand(newBot.getId()).toUri();
		return ResponseEntity.created(uri).body(new BotDTO(newBot));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<BotDTO> findById(@PathVariable UUID id) {

		Optional<Bot> optionalBot = botService.findById(id);

		if(optionalBot.isPresent())
			return ResponseEntity.ok(new BotDTO(optionalBot.get()));

		return ResponseEntity.notFound().build();
	}
}
