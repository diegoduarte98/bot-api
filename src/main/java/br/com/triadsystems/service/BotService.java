package br.com.triadsystems.service;

import br.com.triadsystems.model.Bot;

import java.util.Optional;
import java.util.UUID;

public interface BotService {

    Bot save(Bot bot);

    Optional<Bot> findById(UUID id);
}
