package br.com.triadsystems.service;

import br.com.triadsystems.model.Message;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MessageService {

    Message save(Message message);

    Optional<Message> findById(UUID id);

    List<Message> findByConversationId(UUID conversationId);
}
