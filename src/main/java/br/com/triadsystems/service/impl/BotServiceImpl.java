package br.com.triadsystems.service.impl;

import br.com.triadsystems.model.Bot;
import br.com.triadsystems.repository.BotRepository;
import br.com.triadsystems.service.BotService;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class BotServiceImpl implements BotService {

    private BotRepository botRepository;

    public BotServiceImpl(BotRepository botRepository) {
        this.botRepository = botRepository;
    }

    public Bot save(Bot bot) {
        return botRepository.save(bot);
    }

    @Override
    public Optional<Bot> findById(UUID id) {
        return botRepository.findById(id);
    }
}
