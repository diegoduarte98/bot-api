package br.com.triadsystems.service.impl;

import br.com.triadsystems.model.Message;
import br.com.triadsystems.repository.MessageRepository;
import br.com.triadsystems.service.MessageService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MessageServiceImpl implements MessageService {

    private MessageRepository messageRepository;

    public MessageServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public Message save(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Optional<Message> findById(UUID id) {
        return messageRepository.findById(id);
    }

    @Override
    public List<Message> findByConversationId(UUID conversationId) {
        return messageRepository.findByConversationId(conversationId);
    }
}
