package br.com.triadsystems.controller;

import br.com.triadsystems.BotApiApplicationTests;
import br.com.triadsystems.controller.form.BotForm;
import br.com.triadsystems.model.Bot;
import br.com.triadsystems.repository.BotRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BotControllerTest extends BotApiApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BotRepository botRepository;

    @Test
    public void deve_retornar_http_status_201_ao_salvar_bot() throws Exception {

        BotForm form = new BotForm("Meu Bot");

        this.mockMvc.perform(
                post("/bots")
                        .content(asJsonString(form))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", containsString("Meu Bot")));
    }

    @Test
    public void deve_retornar_http_status_400_ao_salvar_bot_name_nulo() throws Exception {

        BotForm form = new BotForm(null);

        this.mockMvc.perform(
                post("/bots")
                        .content(asJsonString(form))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[*].message", hasItem("must not be null")));
    }

    @Test
    public void deve_retornar_http_status_200_ao_buscar_bot_por_id() throws Exception {

        Bot bot = botRepository.save(new Bot(null, "Meu Bot"));

        this.mockMvc.perform(
                get("/bots/" + bot.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deve_retornar_http_status_404_ao_buscar_bot_por_id_inexistente() throws Exception {

        this.mockMvc.perform(
                get("/bots/901620bd-2d13-453b-81da-d46afe7cec26")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
